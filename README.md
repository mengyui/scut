# README

[![Build Status](http://scutgame.com/images/passing.png?branch=6.7.9.11)](http://scutgame.com/download/)


## Scut是什么?

欢迎使用 Scut
，这是一款免费开源的游戏服务器引擎，适用于开发AVG、SLGRPG、MMOG等类型的网络游戏，同时支持Http、WebSocket和Socket协议通讯，支持Window、Mac和Linux多种平台部署，支持Redis内存数据库和Microsoft
SQL、MySql数据库；服务器引擎框架基于C#编写，游戏逻辑层可以选择使用C#、Python和Lua多种脚本进行开发，支持热更新的方式部署；客户端可以使用Coscos2d、Unity3d、FlashAir与服务器引擎对接；提供了丰富的中间件，可以简单快捷的搭建您的游戏。


## 帮助文档

* [Wiki](http://git.oschina.net/scutgame/Scut/wikis/home)

## 开源协议

参见根目录`LICENSE`文件。
